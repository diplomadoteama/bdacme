/******************************************************************************
**				   	        CREATION OF STORED PROCEDURES	   				 **
*******************************************************************************/
/******************************************************************************
**  Author: Marvin Mendia													 **
**  Date: 06/28/2018														 **
*******************************************************************************
**                            Change History								 **
*******************************************************************************
**   Date:				 Author:                         Description:		 **
** --------			-------------        ----------------------------------- **
** 06/28/2018		Marvin Mendia			Initial version					 **
*******************************************************************************/
USE DWAcme
GO
PRINT 'Start of Script Execution....';
GO
/******************************************************************************
**				 Name: SP_ReportEventualityEmployeeTraining					 **
**				 Desc: Insert Register Area table							 **
**				 Autor: Marvin Mendia										 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_ReportEventualityEmployeeTraining]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_ReportEventualityEmployeeTraining]
END
GO
CREATE PROCEDURE [dbo].[SP_ReportEventualityEmployeeTraining]
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	SELECT feven.[typeEvent]
			,[EmployeeName]= dimE.[Name]
			,[Training] = dimT.[Name]
	FROM [dbo].[FactEventuality] feven
	INNER JOIN [dbo].[DimEmployee] dimE 
	ON (feven.IdEmployee = dimE.IdEmployee)
	INNER JOIN [dbo].[DimTraining] dimT 
	ON (feven.IdTraining = dimT.IdTraining)
END
GO
PRINT 'Procedure [dbo].[SP_ReportEventualityEmployeeTraining] created';
GO
/*

EXECUTE [dbo].[SP_ReportEventualityEmployeeTraining] ;
*/