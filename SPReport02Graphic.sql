/******************************************************************************
**				   	        CREATION OF STORED PROCEDURES	   				 **
*******************************************************************************/
/******************************************************************************
**  Author: Marvin Mendia													 **
**  Date: 06/28/2018														 **
*******************************************************************************
**                            Change History								 **
*******************************************************************************
**   Date:				 Author:                         Description:		 **
** --------			-------------        ----------------------------------- **
** 06/28/2018		Marvin Mendia			Initial version					 **
*******************************************************************************/
USE DWAcme
GO
PRINT 'Start of Script Execution....';
GO
/******************************************************************************
**				 Name: SP_ReportEventualityEmployeeTrainingGraphic			 **
**				 Desc: Insert Register Area table							 **
**				 Autor: Marvin Mendia										 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_ReportEventualityEmployeeTrainingGraphic]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_ReportEventualityEmployeeTrainingGraphic]
END
GO
CREATE PROCEDURE [dbo].[SP_ReportEventualityEmployeeTrainingGraphic]
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	SELECT countEnventuality = COUNT(dimE.[Name])
			,[EmployeeName]= dimE.[Name]
	FROM [dbo].[FactEventuality] feven
	INNER JOIN [dbo].[DimEmployee] dimE 
	ON (feven.IdEmployee = dimE.IdEmployee)
	INNER JOIN [dbo].[DimTraining] dimT 
	ON (feven.IdTraining = dimT.IdTraining)
	group by dimE.[Name];
END
GO
PRINT 'Procedure [dbo].[SP_ReportEventualityEmployeeTrainingGraphic] created';
GO
/*

EXECUTE [dbo].[SP_ReportEventualityEmployeeTraining] ;
*/