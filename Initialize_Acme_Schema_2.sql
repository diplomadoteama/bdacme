/******************************************************************************
**  Name: Script SQL Data Base Initialize Acme
**
**  Authors:	Marvin Dickson Mendia Calizaya
**
**  Date: 06/19/2018
*******************************************************************************
**                            Change History
*******************************************************************************
**   Date:          Author:                         Description:
** --------     -------------     ---------------------------------------------
** 06/19/2018   Marvin Mendia		Initial version
** 06/22/2018   Miguel Lopez        Employee Initialization Data
** 06/22/2018   Marcelo Vargas		Initialization Data, contract and type_contract table
** 06/25/2018	J Marcelo Equise	Inserts for TypeEvent, InjuryType, InjuryPart and Eventuality
** 06/25/2018	  Alfredo Colque	    Inserts for Audit and SafetyRule
*******************************************************************************/
USE Acme;

SET XACT_ABORT ON;
SET NOCOUNT ON;

BEGIN TRANSACTION;


PRINT 'Insert data into the Eventuality table...';
INSERT INTO [dbo].[Eventuality](created_on, dateEvent, [description], typeEvent_id, injuryType_id, injuryPart_id, employee_id, projectArea_id)
VALUES ('2018-06-25', '2018-06-13', 'Sin mayores detalles', 1, 5, 11, 2, 9);
INSERT INTO [dbo].[Eventuality](created_on, dateEvent, [description], typeEvent_id, injuryType_id, injuryPart_id, employee_id, projectArea_id)
VALUES ('2018-06-25', '2018-06-13', 'Sin mayores detalles', 2, 6, 12, 3, 9);
PRINT 'TypeEvent table done...';

--ITEM TYPE
PRINT 'Insert data into the Item table...';
INSERT INTO [dbo].[ItemType]([create_On],[update_On] ,[version] ,[name],[description],[ModifiedBy])
VALUES (GETDATE() ,GETDATE(),2,'Seguridad','Item de equipo de seguridad',3);
PRINT 'ItemType table done...';



--SUBCATEGORY
PRINT 'Insert data into the SubCategory table...';
INSERT INTO [dbo].[SubCategory]([create_On],[update_On],[version],[name],[CategoryID],[description])
VALUES (GETDATE()  ,GETDATE() ,1,'Sistemas de suministro de aire',1,'Un item de subcategoria de seguridad');
PRINT 'SubCategory table done...';



--CATEGORY
PRINT 'Insert data into the Category table...';
INSERT INTO [dbo].[Category]([create_On],[update_On],[version],[name],[description],[SubCategoryID])
VALUES(GETDATE(),GETDATE(),1,'	Cazadoras azulina y amarillas','Items de seguridad',2);
PRINT 'Category table done...';



--ITEM
PRINT 'Insert data into the Item table...';
INSERT INTO [dbo].[Item]([create_On],[update_On],[version],[name],[description],[ModifiedBy],[CategoryID],[SubCategoryID],[ItemTypeID])
VALUES (GETDATE(),GETDATE(),1,'Botas de Hule','Protección en los pies con botas de Hule y PVC de gran calidad para diversos',1,2,1,1);
INSERT INTO [dbo].[Item]([create_On],[update_On],[version],[name],[description],[ModifiedBy],[CategoryID],[SubCategoryID],[ItemTypeID])
VALUES (GETDATE(),GETDATE(),1,'Casco','equipo de seguridad',1,2,1,1);
PRINT 'Item table done...';

COMMIT TRANSACTION;