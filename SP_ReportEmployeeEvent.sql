/*******************************************************************************/
USE DWAcme
GO
PRINT 'Start of Script Execution....';
GO
/******************************************************************************
**				 Name: SP_ReportEmployeeTypeEventGraphic									 **
**				 Desc: stored procedure report of employees y events    	 **
**				 Autor: Marcelo Vargas									 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_ReportEmployeeTypeEventGraphic]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_ReportEmployeeTypeEventGraphic]
END
GO
CREATE PROCEDURE [dbo].[SP_ReportEmployeeTypeEventGraphic]
(
	@nameEmployee varchar(50),
	@nameTypeEvent varchar(50),
	@idProject integer = 1
	-- @nameProyect varchar(50)
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	SELECT [EmployeeName]= dimE.[Name]
		  , project= dimP.Name
		  ,[typeEvent]=feven.typeEvent
		  ,countEvent =  COUNT(feven.typeEvent)
		  
		  
	FROM [dbo].[FactEventuality] feven
	INNER JOIN [dbo].[DimEmployee] dimE ON (feven.IdEmployee = dimE.IdEmployee)
	INNER JOIN [dbo].[DimProject] dimP ON (feven.IdProject = dimP.IdProject)

	WHERE  dimE.name LIKE @nameEmployee+'%' 
	      and feven.typeEvent LIKE  @nameTypeEvent
		--  AND dimP.Name LIKE @nameProyect
		  And feven.IdProject = @idProject
	GROUP BY feven.typeEvent, dimE.[Name],dimP.Name
	--,feven.typeEvent;
	END

GO
PRINT 'Procedure [dbo].[SP_ReportEmployeeTypeEventGraphic] created';
GO

