/******************************************************************************
**  Date: 06/28/2018														 **
*******************************************************************************
**                            Change History								 **
*******************************************************************************
**   Date:				 Author:                         Description:		 **
** --------			-------------        ----------------------------------- **
** 06/28/2018		Marvin Mendia			Initial version					 **
*******************************************************************************/
USE DWAcme
GO
PRINT 'Start of Script Execution....';
GO
/******************************************************************************
**				 Name: SP_ReportEventualityGraphicBetween					 **
**				 Desc: Report Eventuality Graphic							 **
**				 Autor: Marvin Mendia										 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_ReportEventualityGraphicBetween]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_ReportEventualityGraphicBetween]
END
GO
CREATE PROCEDURE [dbo].[SP_ReportEventualityGraphicBetween]
(
	@startDate DATETIME,
	@endDate DATETIME
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	SELECT CountEventuality = COUNT(dimE.[Name])
		  ,[EmployeeName]= dimE.[Name]
		  ,dateEvent
	FROM [dbo].[FactEventuality] feven
	INNER JOIN [dbo].[DimEmployee] dimE 
	ON (feven.IdEmployee = dimE.IdEmployee)
	INNER JOIN [dbo].[DimProject] dimP 
	ON (feven.IdProject = dimP.IdProject)
	INNER JOIN [dbo].[DimTraining] dimT 
	ON (feven.IdTraining = dimT.IdTraining)
	WHERE feven.dateEvent BETWEEN @startDate AND @endDate
	GROUP BY dimE.[Name],dateEvent;
	END
GO
PRINT 'Procedure [dbo].[SP_ReportEventualityGraphicBetween] created';
GO
/*
DECLARE @startDate DATETIME;
DECLARE @endDate DATETIME;

EXECUTE [dbo].[SP_ReportEventualityGraphicBetween] @startDate ='2018-06-12'
													,@endDate ='2018-06-13';
*/