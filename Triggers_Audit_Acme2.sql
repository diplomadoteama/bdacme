/******************************************************************************
**				 Name: TG_Eventuality(Audit)_Delete &  TG_Employee(Audit)_Delete**
**				 Author: Alfredo Colque										 **
*******************************************************************************/
USE Acme
GO
PRINT 'Start of Script Execution....';
GO

IF EXISTS (SELECT * FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[TG_Eventuality(Audit)_Delete]')
		AND type in (N'TR'))
BEGIN
	DROP TRIGGER [dbo].[TG_Eventuality(Audit)_Delete]
END
GO
CREATE TRIGGER [dbo].[TG_Eventuality(Audit)_Delete]
ON [dbo].[Eventuality]
FOR DELETE
AS
BEGIN
  IF TRIGGER_NESTLEVEL(@@ProcID) > 1
    RETURN

  SET NOCOUNT ON;
  SET XACT_ABORT ON;

  DECLARE @CurrDate DATETIME = GETUTCDATE();
    INSERT INTO dbo.AuditHistory(TableName,
                                 ColumnName,
                                 ID,
                                 Date,
                                 OldValue,
                                 NewValue,
								 ModifiedBy)
    SELECT TableName    = 'Eventuality',
           ColumnName   = 'eventuality_id',
           ID1          = d.id,
           Date         = @CurrDate,
           OldValue     = ISNULL(CONVERT(VARCHAR(100), d.[id]),'N/A'),
           NewValue     = 'Delete',
		   ModifiedBy   = 100

    FROM deleted d
END
GO
PRINT 'Trigger [TG_Eventuality(Audit)_Delete] created';

GO



IF EXISTS (SELECT * FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[TG_Employee(Audit)_Delete]')
		AND type in (N'TR'))
BEGIN
	DROP TRIGGER [dbo].[TG_Employee(Audit)_Delete]
END
GO
CREATE TRIGGER [dbo].[TG_Employee(Audit)_Delete]
ON [dbo].[Employee]
FOR DELETE
AS
BEGIN
  IF TRIGGER_NESTLEVEL(@@ProcID) > 1
    RETURN

  SET NOCOUNT ON;
  SET XACT_ABORT ON;

  DECLARE @CurrDate DATETIME = GETUTCDATE();
    INSERT INTO dbo.AuditHistory(TableName,
                                 ColumnName,
                                 ID,
                                 Date,
                                 OldValue,
                                 NewValue,
								 ModifiedBy)
    SELECT TableName    = 'Employee',
           ColumnName   = 'employee_id',
           ID1          = d.id,
           Date         = @CurrDate,
           OldValue     = ISNULL(CONVERT(VARCHAR(100), d.[id]),'N/A'),
           NewValue     = 'Delete',
		   ModifiedBy   = ISNULL(d.ModifiedBy,100)

    FROM deleted d
END
GO
PRINT 'Trigger [TG_Employee(Audit)_Delete] created';

GO