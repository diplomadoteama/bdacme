USE Acme
-----Area------
/*
DECLARE @Code VARCHAR(10);
DECLARE @Name VARCHAR(50);
DECLARE @ModifiedBy INT;

EXECUTE [dbo].[SP_InsertArea] @Code ='Elc-01'
							 ,@Name ='Electricity'
							 ,@ModifiedBy = 100;
*/
 /*
DECLARE @IdArea INT;
DECLARE @Code VARCHAR(10);
DECLARE @Name VARCHAR(30);
DECLARE @ModifiedBy INT;

EXECUTE [dbo].[SP_UpdateArea] @IdArea = 1
								,@Code ='Elc-01'
								,@Name = 'Electricity'
								,@ModifiedBy = 100;
*/
/*
DECLARE @IdArea INT;
EXECUTE [dbo].[SP_DeleteArea] @IdArea = 2
*/

-----Training------
/*
DECLARE @Code VARCHAR(10);
DECLARE @Name VARCHAR(50);
DECLARE @instructor VARCHAR(50);
DECLARE @Area_Id INT;
DECLARE @ModifiedBy INT;

EXECUTE [dbo].[SP_InsertTraining] @Code ='Elceeeeee'
								  ,@Name ='Electricidad Industrial'
								  ,@Instructor ='Romero Carlos'
								  ,@Area_Id =1
								  ,@ModifiedBy = 100;
*/
 /*
DECLARE @IdTraining INT;
DECLARE @Code VARCHAR(10);
DECLARE @Name VARCHAR(30);
DECLARE @Instructor VARCHAR(50);
DECLARE @Area_Id INT;
DECLARE @ModifiedBy INT;

EXECUTE [dbo].[SP_UpdateTraining] @IdTraining = 2
								,@Code ='Elc-002'
								,@Name = 'qimbol'
								,@Instructor = 'Romero Carlos'
								,@Area_Id = 1
								,@ModifiedBy = 100;
*/
/*
DECLARE @IdTraining INT;
EXECUTE [dbo].[SP_DeleteTraining] @IdTraining = 1
*/
--------------------------PROJECTS -------------------------------
/*
EXECUTE [dbo].[SP_GetProjectById] @id = '1'
*/
/*
EXECUTE [dbo].[SP_GetAllProjects]
*/


/*
DECLARE @name VARCHAR(250);
DECLARE @description VARCHAR(1000);
DECLARE @date_start DATE;
DECLARE @date_end DATE;

EXECUTE [dbo].[SP_InsertProject] @name ='test project'
							 ,@description ='test description'
							 ,@date_start = '2018-05-10'
							 ,@date_end = '2018-06-10';
*/


/*
EXECUTE [dbo].[SP_DeleteProject] @id = '25'
*/


/*
DECLARE @id INT;
DECLARE @name VARCHAR(250);
DECLARE @description VARCHAR(1000);
DECLARE @date_start DATE;
DECLARE @date_end DATE;

EXECUTE [dbo].[SP_UpdateProject] @id = 24
								,@name ='test project update'
							    ,@description ='test description'
							    ,@date_start = '2018-05-10'
							    ,@date_end = '2018-06-10';
*/

---------------------------Project Areas--------------------------
/*
DECLARE @project_id INT;
DECLARE	@area_id INT;
DECLARE	@estado VARCHAR(50);

EXECUTE [dbo].[SP_InsertProject_Area] @project_id = 24
							          ,@area_id = 4
							          ,@estado = 'Terminado';
							 
*/

/*
DECLARE @Id INT
DECLARE @project_id INT;
DECLARE	@area_id INT;
DECLARE	@estado VARCHAR(50);

EXECUTE [dbo].[SP_UpdateProject_Area] @id = 28
									  ,@project_id = 24
							          ,@area_id = 3
							          ,@estado = 'Terminado';
							 
*/


/*
EXECUTE [dbo].[SP_DeleteProject_Area] @Id = '28'
*/


/*
EXECUTE [dbo].[SP_GetProjectAreaById] @id = '1'
*/
/*
EXECUTE [dbo].[SP_GetAllProject_Area]
*/


------------------------------------------------------------------

 /*
DECLARE @IdPosition INT;
DECLARE @Name VARCHAR(30);
DECLARE @Role_Id INT;
DECLARE @ModifiedBy INT;

EXECUTE [dbo].[SP_UpdatePosition] @IdPosition = 11
								,@Name = 'rrrr'
								,@Role_Id = 1
								,@ModifiedBy = 100;
*/

/*
DECLARE @IdRole INT;
EXECUTE [dbo].[SP_DeleteRole] @IdRole = 12
*/