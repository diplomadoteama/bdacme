--#Base de datos
:r C:\Scripts\BD_ACME.sql
:r C:\Scripts\Stored_Procedures_Acme.sql
:r C:\Scripts\Triggers_Audit_Acme.sql
:r C:\Scripts\Triggers_Audit_Acme2.sql
:r C:\Scripts\SP_GetAuditHistory.sql

--#Data ware house
:r C:\Scripts\Create_DWAcme_Schema.sql
:r C:\Scripts\SPReport01.sql
:r C:\Scripts\SPReport01Graphic.sql
:r C:\Scripts\SPReport02.sql
:r C:\Scripts\SPReport02Graphic.sql
:r C:\Scripts\SP_ReportEmployeeEvent.sql
:r C:\Scripts\SP_ReportEventualityGraphic.sql
:r C:\Scripts\SP_ReportTypeEventualityForEmployee.sql
:r C:\Scripts\Script_Funtion_StoredProcedure_For_DW.sql

-- #datos de inicializacion bd Acme
:r C:\Scripts\Initialize_Acme_Schema.sql
:r C:\Scripts\Initialize_Acme_Schema_2.sql
:r C:\Scripts\Script_CreateETL_Job(Acme).sql
-- :r C:\Scripts\execute_DWAcme.sql  -- solo se ejecuta si no esta corriendo ETL_Job(Acme)