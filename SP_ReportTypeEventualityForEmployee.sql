/******************************************************************************
**				   	        CREATION OF STORED PROCEDURES	   				 **
*******************************************************************************/
/******************************************************************************
**  Author: Marvin Mendia													 **
**  Date: 07/07/2018														 **
*******************************************************************************
**                            Change History								 **
*******************************************************************************
**   Date:				 Author:                         Description:		 **
** --------			-------------        ----------------------------------- **
** 07/07/2018		Marvin Mendia			Initial version					 **
*******************************************************************************/
USE DWAcme
GO
PRINT 'Start of Script Execution....';
GO
/******************************************************************************
**				 Name: SP_ReportTypeEventualityForEmployee					 **
**				 Desc: stored procedure type Eventuality for employee		 **
**				 Autor: Marvin Mendia										 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_ReportTypeEventualityForEmployee]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_ReportTypeEventualityForEmployee]
END
GO
CREATE PROCEDURE [dbo].[SP_ReportTypeEventualityForEmployee]
(
	@employeeId INT
)
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	SELECT feven.[typeEvent]
			,numberEventType = COUNT(*)
	FROM [dbo].[FactEventuality] feven
	INNER JOIN [dbo].[DimEmployee] dimE 
	ON (feven.IdEmployee = dimE.IdEmployee)
	WHERE feven.IdEmployee = @employeeId
	GROUP BY feven.[typeEvent];
END
GO
PRINT 'Procedure [dbo].[SP_ReportTypeEventualityForEmployee] created';
GO
/*
DECLARE @employeeId INT
EXECUTE [dbo].[SP_ReportTypeEventualityForEmployee]  @employeeId = 1;
*/