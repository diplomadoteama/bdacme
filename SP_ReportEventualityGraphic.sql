/******************************************************************************
**				   	        CREATION OF STORED PROCEDURES	   				 **
*******************************************************************************/
/******************************************************************************
**  Author: Marvin Mendia													 **
**  Date: 07/07/2018														 **
*******************************************************************************
**                            Change History								 **
*******************************************************************************
**   Date:				 Author:                         Description:		 **
** --------			-------------        ----------------------------------- **
** 07/07/2018		Marvin Mendia			Initial version					 **
*******************************************************************************/
USE DWAcme
GO
PRINT 'Start of Script Execution....';
GO
/******************************************************************************
**				 Name: SP_ReportEventuality									 **
**				 Desc: stored procedure Eventuality of employees			 **
**				 Autor: Marvin Mendia										 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[SP_ReportEventuality]') 
		AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [dbo].[SP_ReportEventuality]
END
GO
CREATE PROCEDURE [dbo].[SP_ReportEventuality]
AS
SET XACT_ABORT ON;
SET NOCOUNT ON;
BEGIN
	SELECT [EmployeeName]= x.EmployeeName
			,EventualityNumber = count(*)
			,EventualityType = x.EventualityType
			FROM ((SELECT feven.IdEventuality,[EmployeeName]= dimE.[Name]
					,EventualityType = feven.typeEvent
					FROM [dbo].[FactEventuality] feven
					INNER JOIN [dbo].[DimEmployee] dimE 
					ON (feven.IdEmployee = dimE.IdEmployee)
					INNER JOIN [dbo].[DimProject] dimP 
					ON (feven.IdProject = dimP.IdProject)
					INNER JOIN [dbo].[DimTraining] dimT 
					ON (feven.IdTraining = dimT.IdTraining)
					LEFT JOIN (SELECT distinct IdEventuality, IdEmployee, IdProject
								FROM FactEventuality) a on (a.IdEventuality = feven.IdEventuality)
								WHERE feven.typeEvent = 'Accidente'
								GROUP BY feven.IdEventuality,dimE.[Name],feven.typeEvent)
 
					UNION

					(SELECT feven.IdEventuality,[EmployeeName]= dimE.[Name]
							,EventualityType = feven.typeEvent
					FROM [dbo].[FactEventuality] feven
					INNER JOIN [dbo].[DimEmployee] dimE 
					ON (feven.IdEmployee = dimE.IdEmployee)
					INNER JOIN [dbo].[DimProject] dimP 
					ON (feven.IdProject = dimP.IdProject)
					INNER JOIN [dbo].[DimTraining] dimT 
					ON (feven.IdTraining = dimT.IdTraining)
					LEFT JOIN (SELECT distinct IdEventuality, IdEmployee, IdProject
								FROM FactEventuality) b on (b.IdEventuality= feven.IdEventuality)
								WHERE feven.typeEvent = 'Incidente'
								GROUP BY feven.IdEventuality,dimE.[Name],feven.typeEvent)) x
	GROUP BY x.EmployeeName,x.EventualityType;
END
GO
PRINT 'Procedure [dbo].[SP_ReportEventuality] created';
GO
/*

EXECUTE [dbo].[SP_ReportEventuality] ;
*/